#Gulp Template

##Requirements
You will need node.js and npm installed.

##Installing
Install npm dependencies.  These are listed in the package.json file.  The first one is gulp, which tells NPM to install the latest version of gulp.  

```
# npm install
```

##Tasks
Useful gulp tasks thus far written:


```
# gulp
```
gulp default task is used for development:
1. concatenates all js files into one output file, currently named 'sum-of-all.js'.
2. compiles all "*.scss" into styles.css (styles.scss imports all other scss files we need, so this should be the one and only css file of ours we need to edit).
3. concatenates all .css files into one output file, currently named 'sum-of-all.css'
4. watches for changes to "*.scss" and "*.js".  If changes are detected, recompiles and concatenates the appropriate files.

```
# gulp watch
```
Just watches for changes without compiling or concatenating anything in advance.  Upon detecting changes, will recompile and concatenate as appropriate.

```
# gulp dist
```
meant for distribution.  Does not watch for changes (it's rather slow).  Concatenates, compiles, and uglifys (minifys) code, puts sum-of-all.min.js and sum-of-all.css into /dist folder.


Adding new js and css files to the build:
In gulpfile.js you can add source files to the jsScripts and cssFiles arrays.  The order matters, so if something is dependent upon something else, add it afterwards.  Currently, there are no catchall *.js or *.css behaviors in there, but you can use gulp glob syntax (google it)) to define what files it pulls in, if you want to get get multiple files, similar to a regular expression.  You can also change the source and destination directories by editing the variables at the beginning of the file.  It's just a js file, so you can add all sorts of javascript processing, if you want.  
The basics of how the tasks are defined is pretty simple.  This tutorial was useful to me: <https://semaphoreci.com/community/tutorials/getting-started-with-gulp-js>.
